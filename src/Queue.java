public class Queue {

    private String Tabarr[];
    private int front;
    private int rear;
    private int capacite;      // Pour connaitre la capacité du tableau
    private int size;         // Pour savoir la taille en temps reel du tableau
    private int size() {
        return size;
    }
    public Boolean isEmpty()
    {
        return (size() == 0);
    }
    public Boolean isFull()
    {
        return (size() == capacite);
    }

    Queue(int size)
    {
        Tabarr = new String[size];
        capacite = size;
        front = 0;
        rear = -1;

    }

    public void add(String Value)
    {

        if (isFull())
        {
            System.out.println("La liste est full");
        }
        else {

            System.out.println("Objet inseré  :" + Value);
        }
        rear = (rear + 1)  ;
        Tabarr[rear] = Value;
        size++;
    }

    public void delete()
    {

        if (isEmpty())
        {
            System.out.println("La liste est vide");
        }
        else {

            System.out.println("Objet supprimé " + Tabarr[front]);
        }
        front = (front + 1);
        size--;
    }

    public String peek()
    {
        if (isEmpty())
        {
            System.out.println("La Queue est vide");
        }
        return Tabarr[front];
    }



}
